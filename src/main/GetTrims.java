
package main;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author aharris
 */
public class GetTrims {
    
    public GetTrims( String trimsFile, String avsScript ){
        
        String trimsStr = "";
        String [] lineArr;
        
        try {
            
            List<String> readLines = FileUtils.readLines( FileUtils.getFile(trimsFile), StandardCharsets.ISO_8859_1 );
            
            for( String line : readLines ){
                
                //lineArr[0] = frame number
                //lineArr[1] = trim start/end flag
                lineArr = line.trim().split( "\\s+" );
                
                if( lineArr[1].equals("S") )
                    trimsStr += "Trim("+ lineArr[0];
                else
                    trimsStr += ","+ lineArr[0] +") ++ ";
            }
            
            trimsStr = trimsStr.replaceAll( " \\+\\+ $", "" );
            
            readLines = FileUtils.readLines( FileUtils.getFile(avsScript), StandardCharsets.ISO_8859_1 );
            readLines.add( trimsStr );
            
            FileUtils.writeLines( FileUtils.getFile(avsScript), readLines, false );
            
        } catch (IOException ex) {
            Logger.getLogger(GetTrims.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private static void printUsage( ){
        
        System.out.println( "\n\nUsage: GetTrims <input> <output avs script>\n" );
    }
    
    public static void main( String ... args ){
        
        if( args.length < 2 || args.length > 2 ){
            printUsage( );
            System.exit(1);
        }
        
        if( !new File(args[0]).exists() ){
            System.out.println( "\nInput file not found: "+ args[0] );
            System.exit(1);
        }
        if( !new File(args[1]).exists() ){
            System.out.println( "\nAvs script not found: "+ args[1] );
            System.exit(1);
        }
        
        new GetTrims( args[0], args[1] );
    }
}
